all: cliente servidor

cliente:
	gcc Cliente.c -o client

servidor:
	gcc Servidor.c -o servidor

.PHONY clean:
	rm client servidor 
