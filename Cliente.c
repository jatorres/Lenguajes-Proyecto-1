#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <netinet/in.h>



//Funcion que da el socket ya sabiendo el usuario del archivo de usuarios
int getport_aux(char linea[]){
	int i = strlen(linea);
	int retorno = 0;
	int base_decimal = 1; 
	i-=2;// Caracter enter
	int j = 0;		
	while(linea[i]!='@'){			
		retorno+= (linea[i]-48)*base_decimal;
		base_decimal*=10;
		i--;
	}
	return retorno;
	
}


// pide un descriptor asociado a un usuario en el archivo de usurios conectados
int getport(){	
	FILE *file = fopen("config.fk", "r");
    char line[100];
	int port;
    fgets(line, sizeof line, file);	
	port = getport_aux(line);
	fclose(file);
	return port;
}

void encriptar (char *mensaje){
	int i = 0;
	do{
	 mensaje[i]++;
	 }while(mensaje[++i]);
	}

void desencriptar (char *mensaje){
	int i = 0;
	do{
	 mensaje[i]--;
	 }while(mensaje[++i]);
	}

int isValido(char mensaje[], int len){
	if(strcmp(mensaje,"a!\n") == 0 || strcmp(mensaje, "q!\n") == 0){
		return 1;
	}
	
	for(int i=0; i<len; i++){
		if(mensaje[i] == '@'){
			return 1;
		}
	}
	return -1;
}


#define COLOR_ROJO     "\x1b[31m"
#define COLOR_AMARILLA  "\x1b[33m"
#define COLOR_AZUL    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"



int main()
{


//Declarando socket

int cliente_socket;
cliente_socket = socket(AF_INET, SOCK_STREAM, 0 ); //AF_INET = socket internet //SOCK_STREAM = Protocolo TCP // 0 ?

//Direccion
struct sockaddr_in direccion_cliente; 
direccion_cliente.sin_family = AF_INET; // familia donde se va conectar
direccion_cliente.sin_port = htons(getport()); // puerto donde pasara la info, htons estructura el puerto para solo pasarle el numero
direccion_cliente.sin_addr.s_addr = INADDR_ANY; // donde se encuentra el servidor(local, igual lo estrucuta, puerto 0000)





size_t largo_max_usuario = 20;
char *usuario = (char*) malloc(largo_max_usuario * sizeof(char));
char *usuario_destino = (char*) malloc(largo_max_usuario * sizeof(char));

size_t largo_usuario;
size_t largo_usuario_destino;
printf("Digite su usuario\nUsuario: ");
largo_usuario = getline(&usuario,&largo_max_usuario,stdin);
usuario[largo_usuario-1] = '\0';
encriptar(usuario);

int estado_conexion = connect(cliente_socket, (struct sockaddr * ) &direccion_cliente, sizeof(direccion_cliente));

if (estado_conexion == -1){
	printf("Error \n");
	close(1);
}else{
	printf("Conexion establecida \n");
}


send(cliente_socket, usuario, largo_usuario, 0);



size_t largo_max_buffer = 1024;
char *mensaje = (char*) malloc(largo_max_buffer * sizeof(char));
size_t largo_del_mensaje;



printf(COLOR_AMARILLA "\nDigite\n: usuario_destino@mensaje - para enviar mensaje\na! - para ver esta seccion de nuevo\nq! - Para salir\n" COLOR_RESET);	
	



	pid_t pid = fork();
	if(pid == 0){
		while(1){		
			largo_del_mensaje = getline(&mensaje,&largo_max_buffer,stdin);
			if(isValido(mensaje, largo_del_mensaje) == 1){
				mensaje[largo_del_mensaje] = '\0';
				encriptar(mensaje);
				send(cliente_socket, mensaje, largo_del_mensaje, 0);
				desencriptar(mensaje);
				if(strcmp(mensaje,"q!\n") == 0){
					close(cliente_socket);
					printf("\nDesconectado del servidor \n");
					exit(1);
	
				}
				if(strcmp(mensaje,"a!\n") == 0){
					printf(COLOR_AMARILLA "\nDigite\n: usuario_destino@mensaje - para enviar mensaje\na! - para ver esta seccion de nuevo\nq! - Para salir\n" COLOR_RESET);
				}
				bzero(mensaje,strlen(mensaje));
			}else{
				printf(COLOR_ROJO "Formato invalido\nRecuerde que digitando a! se le mostraran las opciones validas" COLOR_RESET "\n");
			}
			
			
		}
	}else{
		int len_mensaje;
		while(1){
			bzero(mensaje,strlen(mensaje));		
			len_mensaje = recv(cliente_socket, mensaje, 1024, 0);
			mensaje[len_mensaje]='\0';
			desencriptar(mensaje);
			if( len_mensaje < 0){
				printf("Error al recibir datos \b");
			}else{
				if(strcmp(mensaje,"q!") == 0){
				exit(1);						
				}
				if(strcmp(mensaje,"a!") != 0){
					printf(COLOR_AZUL "\n%s" COLOR_RESET"\n",mensaje );
				}
				
			}
		}
	}
	
	

	




return 0;
}
