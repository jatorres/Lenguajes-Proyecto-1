#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>

#include <netinet/in.h>
#include <arpa/inet.h>

//Si algun archivo lo borra y arranca de nuevo, esto por el hecho que los usuarios al levantar el servidor no debe quedar ningun registro
void crear_archivos(char ruta[]){
	FILE *puntero_archivo = fopen(ruta, "r");
	if(puntero_archivo != NULL){		
		fclose(puntero_archivo);
		remove(ruta);
	}
	puntero_archivo = fopen(ruta,"w+");
	fclose(puntero_archivo);	
}

//Atoi no me sirvio, entonces un simple conversor, el cual se eligio un buffer de 12 bytes porque la longitud de un descriptor de socket ronda los 8 caracteres
void intTostring(char buffer[12], int numero){
	int contador = 1;
	while(numero>contador){
		contador*=10;
	}

	int puntero = 0;
	while(contador!=1){
		contador/=10;
		buffer[puntero] = numero/contador+48 ;
		numero %= contador;		 
		puntero++;
	}
	buffer[puntero] = '\n';
	buffer[puntero+1] = '\0';
	
}

char* getUsuario (char mensaje[]){
	int largo = strlen(mensaje);
	int j = 0;
	
	while(mensaje[j]!=':' && mensaje[j]!= '@'){
		j++;
	}
	
	
	char *retorno = (char*) malloc(j * sizeof(char)); 
	int i = 0;		
	for(i; i<j; i++){
		retorno[i] = mensaje[i];
	}
	retorno[i] = '\0';
	return retorno; 
	
}

int getU_aux(char linea[]){
	int i = strlen(linea);
	int retorno = 0;
	int base_decimal = 1; 
	i-=2;// Caracter enter
	int j = 0;	
	while(linea[j]!='@'){
		 	j++;	
		}		
	
	while(linea[i]!='@'){			
		retorno+= (linea[i]-48)*base_decimal;
		base_decimal*=10;
		i--;
	}
	return retorno;
	
}

char* getUsuario_por_id(int sock_id){
	FILE *file = fopen(".usuarios_conectados", "r");
    char line[100];
    int id;
    char * usuario = (char *) malloc (30 * sizeof(char));
    while (fgets(line, sizeof line, file) != NULL) 
    {	
		id = getU_aux(line);
		if(id == sock_id){
			fclose(file);
			usuario = getUsuario(line);
			return usuario;
			
		}
    }
	fclose(file);
	return NULL;
}

void encriptar (char *mensaje){
	int i = 0;
	do{
	 mensaje[i]++;
	 }while(mensaje[++i]);
	}

void desencriptar (char *mensaje){
	int i = 0;
	do{
	 mensaje[i]--;
	 }while(mensaje[++i]);
	}


//Copia un string a otra posicion de memoria sin el usuario
void copiarString(int len, char * destino, char original[]){
	int i = 0;
	int j = 0;
	for (i; i<len;i++){
		if(original[i] == 'A'){
			destino[j] = ';';
			j++;
			destino[j] = '!';
			j++;
		}else{
			destino[j] = original[i];
			j++;
		}
			
		
	}
	destino[j] = '\0';
	
}

void concatenar(char * destino, char * usuario, char * mensaje){
	int i = 0;
	while(usuario[i]){
		destino[i]= usuario[i];
		i++;
	}
	int j = 0;
	while(mensaje[j]){
		destino[i] = mensaje[j];
		i++;
		j++;
	}
	destino[i] = '\0';
	
}


//Funcion que los procesos se comuniquen  por medio de los archivos
//modo(1 si es un mensaje - otra cosa es un usuario), ruta(descriptos del archivo), nombre(usuario que registra o a quien se le envía el mensaje)
// descriptor_del_socket(descriptor del socket para registrar, no es necesario para enviar el msj)
// mensaje(mensaje que se quiere enviar, no es necesario si se quiere registrar el usuario)  
int registar (int modo, char ruta[], char nombre[], int descriptor_del_socket, char mensaje[]){
	int retorno = 0;
	FILE *puntero_archivo = fopen(ruta,"a");	
	retorno+= fwrite(nombre, sizeof(char), strlen(nombre), puntero_archivo);
	retorno+=fwrite("@", sizeof(char), 1, puntero_archivo);
	
	if(modo == 1){
		retorno+=fwrite(mensaje, sizeof(char), strlen(mensaje), puntero_archivo);
		retorno+=fwrite("\n", sizeof(char), 1, puntero_archivo);
	}else{
		char descriptor [12];
		intTostring(descriptor,descriptor_del_socket);	
		retorno+=fwrite(descriptor, sizeof(char), strlen(descriptor), puntero_archivo);
	}

	fclose(puntero_archivo);	
	return retorno;
}

//Toma la linea y la desglosa hasta tener el puerto convertido en int y lo devuelve
int getport_aux(char linea[]){
	int i = strlen(linea);
	int retorno = 0;
	int base_decimal = 1; 
	i-=2;// Caracter enter
	int j = 0;		
	while(linea[i]!='@'){			
		retorno+= (linea[i]-48)*base_decimal;
		base_decimal*=10;
		i--;
	}
	return retorno;
	
}


// Da el puerto ubicado en el archivo de configuracion
int getport(){	
	FILE *file = fopen("config.fk", "r");
    char line[100];
	int port;
    fgets(line, sizeof line, file);	
	port = getport_aux(line);
	fclose(file);
	return port;
}

//Funcion que da el socket ya sabiendo el usuario del archivo de usuarios
int getSock_id_aux(char linea[],char usuario[]){
	int i = strlen(linea);
	int retorno = 0;
	int base_decimal = 1; 
	i-=2;// Caracter enter
	int j = 0;	
	while(linea[j]!='@'){
		if(linea[j]!= usuario[j]){
			return -1;
		}else{
		 	j++;	
		}		
	}
	if(j!= strlen(usuario)){
		return -1;
	}
	
	while(linea[i]!='@'){			
		retorno+= (linea[i]-48)*base_decimal;
		base_decimal*=10;
		i--;
	}
	return retorno;
	
}


// pide un descriptor asociado a un usuario en el archivo de usurios conectados
int getSock_id(char archivo[], char usuario[]){	
	FILE *file = fopen(archivo, "r");
    char line[100];
	int id;
    while (fgets(line, sizeof line, file) != NULL) 
    {	
		id = getSock_id_aux(line,usuario);
		if(id > 0){
			fclose(file);
			return id;
		}
    }
	fclose(file);
	return -1;
}



//Da el id independiente del usuario, sino de la linea del archivo
int getID(char linea[]){
	int i = strlen(linea);
	int retorno = 0;
	int base_decimal = 1; 
	i-=2;// Caracter enter y el final
	int j = 0;	
	while(linea[j]!='@'){
			j++;
		}
	while(linea[i]!='@'){			
		retorno+= (linea[i]-48)*base_decimal;
		base_decimal*=10;
		i--;
	}
	return retorno;
}


	
//Mete todos los id de los usuarios conectados a un arreglo	
void getAll_id(char archivo[], int *ids){	
	FILE *file = fopen(archivo, "r");
    char line[100];
	int id;
	int i = 0;
    while (fgets(line, sizeof line, file) != NULL) 
    {	
		id = getID(line);
		ids[i]=id;
		i++;
    }
	fclose(file);
}



// Da cuantos usuarios hay el linea en el momento
int usuarios_conectados(char archivo[]){
	FILE *file = fopen(archivo, "r");
    char line[100];
    int retorno = 0;
	while (fgets(line, sizeof line, file) != NULL) 
    {	
		retorno++;
    }
	fclose(file);
	return retorno;
}




char* getMsg_aux(char linea[],char usuario[]){
	int largo = strlen(linea);
	int j = 0;
	while(linea[j]!='@'){
		if(linea[j]!= usuario[j]){
			return NULL;
		}else{
		 	j++;	
		}		
	}
	if(j != strlen(usuario)){
		return NULL;
	}
	j++;
	largo--;
	int i = 0;
	char *retorno = (char*) malloc( (largo-j) * sizeof(char));
	for(j; j<largo; j++){
		retorno[i] = linea[j];
		i++;
	}
	return retorno;
}  

char* getMsg(char archivo[], char usuario[]){	
	FILE *file = fopen(archivo, "r");
    char line[2500];
    char *retorno;
    while (fgets(line, sizeof line, file) != NULL) 
    {	
		retorno = getMsg_aux(line,usuario);
		if(retorno != NULL){
			fclose(file);
			return retorno;
		}
    }
	fclose(file);
	return NULL;
}



//Mata num_usuarios procesos en el arreglo de procesos
void terminar_procesos(int *ids, int num_usuarios){
	for(int i = 0; i<num_usuarios; i++){
		kill(ids[i], SIGTERM);		
	}

}

void quitar_usuario(char *mensaje){
		int i = 0;
		int j = 0;
		while(mensaje[i]!=':'){
			i++;
		}
		while(mensaje[i]){
			mensaje[j] = mensaje[i];
			j++;
			i++;
		}
		mensaje[j] = mensaje[i];

}





int main(){
	
	
	char archivo_usuarios[] = ".usuarios_conectados";
	
	
	crear_archivos(archivo_usuarios);
	
	int MAX_USUARIOS = 20;	
	
	pid_t *child_pids = (pid_t *) malloc(MAX_USUARIOS * sizeof(pid_t));
	int *ids = (int *) malloc(MAX_USUARIOS * sizeof(int));
	int numero_sockets_conectados = 0;
	
	
	
	
	int puerto_original = getport();
	char mensaje[1024];
	pid_t pid; // variable del proceso
	//Declarando socket
	int socket_servidor, ret_bind, socket_entrante;
	struct sockaddr_in direccion_servidor;
	struct sockaddr_in direccion_entrante;
	socklen_t len_socket;
	
	
	socket_servidor = socket(AF_INET, SOCK_STREAM, 0 ); //AF_INET = socket internet //SOCK_STREAM = Protocolo TCP // 0 ?
	
	
	
	if(socket_servidor < 0){
		printf("Error en la conexion \n");
		exit(1);
	}
	printf("Servidor creado \n");
	//Direccion
	
	
	 
	direccion_servidor.sin_family = AF_INET; // familia donde se va conectar
	direccion_servidor.sin_port = htons(puerto_original); // puerto donde pasara la info, htons estructura el puerto para solo pasarle el numero
	direccion_servidor.sin_addr.s_addr = INADDR_ANY; // donde se encuentra el servidor(local, igual lo estrucuta)

	ret_bind = bind(socket_servidor, (struct sockaddr * ) &direccion_servidor, sizeof(direccion_servidor));
	if (ret_bind < 0){
		printf("Error al levantar el socket \n");
		shutdown(socket_servidor,2);
		exit(1);
	}
	
	if(listen(socket_servidor, 10) == 0){
		printf("Esperando.... \n");
	}else{
		printf("Fatal Error!!!\n");
		shutdown(socket_servidor,2);
		exit(1);
	}
	
	pid_t child_pid;
	
	char *usuario = (char*) malloc(20 * sizeof(char));
	char *mensaje_final = (char*) malloc(1024 * sizeof(char));
	int sock_destino;
	
	while(1){	
		socket_entrante = accept(socket_servidor, (struct sockaddr*) &direccion_entrante, &len_socket);
		if(socket_entrante < 0){
			exit(1);
		}		
		printf("Conexion establecida con: ip - %s : puerto - %d \n",inet_ntoa(direccion_entrante.sin_addr), ntohs(direccion_entrante.sin_port));
		int len_usuario = recv(socket_entrante,usuario,20,0);
		usuario[len_usuario] = '\0';
		printf("Usuario encriptado - %s\n",usuario);
		desencriptar(usuario);
		numero_sockets_conectados = usuarios_conectados(archivo_usuarios);
		if(numero_sockets_conectados != 0){
			terminar_procesos(child_pids, numero_sockets_conectados);
		}
		registar(0,archivo_usuarios,usuario,socket_entrante,"");
		numero_sockets_conectados++;
		getAll_id(archivo_usuarios,ids);
		for(int y = 0; y<numero_sockets_conectados; y++){
			child_pid = fork();
			if(child_pid == 0){
				socket_entrante = ids[y];
				while(1){
					int len_msj=recv(socket_entrante, mensaje, 1024, 0);		
			 		char *mensaje_saliente = (char*) malloc( (len_msj+10) * sizeof(char));
					copiarString(len_msj, mensaje_saliente, mensaje);
					printf("mensaje encriptado - %s\n", mensaje_saliente);
					desencriptar(mensaje_saliente);
			 		if(strcmp(mensaje_saliente,"q!\n") == 0){
						printf("Conexion terminada con %d!!\n", socket_entrante);
						send(socket_entrante, "r\"",2,0);
						kill(getpid(),SIGTERM);			
					}else{
						if(strcmp(mensaje_saliente,"b\"") != 0){
						sock_destino = getSock_id(archivo_usuarios,getUsuario(mensaje_saliente));
						//printf("%s\n",getUsuario_por_id(sock_destino));
						if(sock_destino > 0){
							usuario = getUsuario_por_id(socket_entrante);
							quitar_usuario(mensaje_saliente);
							concatenar(mensaje_final, usuario, mensaje_saliente);
							
							encriptar(mensaje_final);
							send(sock_destino, mensaje_final, len_msj+20, 0);
						}else{
							send(socket_entrante,"FSSPS!VTVBSJP!OP!DPOFDUBEP",26,0);
						}
						}
						
					}   
					
					
					
			 		
					
				}
			
				}else{
					child_pids[y] = child_pid;
				}
	
			}
		}
	
close(socket_servidor); // para de recibir info e enviar info	

return 0;
}
